Name: Nguyễn Văn Trọng.
ID: 1653096.
Mail: 1653096@student.hcmus.edu.vn
Phone: 01677668343

Build bằng Visual Studio 2017(x64)
Em đã cố gắng chia file nhưng trước khi code không chia nên sau này chia phát sinh lỗi mong thầy thông cảm.
Source lưu trữ trên Bitbucket tại: https://bitbucket.org/nvtrong/1653096-ltwin_p3/src/master/
Release đã có trong source.
Mượn source P2(đã được thầy Tuấn cho phép).
Source P2 mượn từ bạn Nguyễn Văn Tú vì P2 của em Select Object chạy không được.

Môn học Lập trình Windows (Windows Programming)

Bài tập project (P3)

Thêm các chức năng cần thiết vào menu và toolbar  : Hoàn thành
Cut : Cut object đang được chọn, lưu vào clipboard 
Copy: Copy object đang được chọn, lưu vào clipboard
Paste: Paste object từ clipboard. Ưu tiên paste object của ứng dụng MyPaint. Nếu không có object
nào của MyPaint thì sẽ paste CF_TEXT.
Delete : Xóa object đang được chọn.
( Đã có cài đặt phím tắt như yêu cầu)

Chức năng Paste: Hoàn thành.
Khi paste một object của ứng dụng MyPaint thì object đó cần được thêm vào danh sách và
quản lý như mọi object bình thường khác.
o Khi paste một CF_TEXT thì sẽ tạo mới một object kiểu Text và thêm vào danh sách của ứng
dụng. Object text mới được gán font, color theo font, color đang được chọn của child
window. Object text mới được gán tọa độ (0, 0).

