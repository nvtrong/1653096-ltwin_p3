#include "stdafx.h"
#include"Object.h"

Object::Object()
{
	A.x = A.y = B.x = B.y = 0;
	type = 0;
	count_str = 0;
	startPoint.x = startPoint.y = 0;
	originalBegin.x = originalBegin.y = originalEnd.x= originalEnd.y=0;
}
void Object::Swap(Object *& tmp)
{
	A.x = tmp->A.x;
	A.y = tmp->A.y;
	B.x = tmp->B.x;
	B.y = tmp->B.y;
	color = tmp->color;
	type = tmp->type;
}


void Object::StartPoint(Point &a)
{
	startPoint.x = a.x;
	startPoint.y = a.y;
	originalBegin = A;
	originalEnd = B;
}

void Object::MovingObj( Point &cur)
{
	A.x = originalBegin.x + cur.x - startPoint.x;
	A.y = originalBegin.y + cur.y - startPoint.y;
	B.x = originalEnd.x + cur.x - startPoint.x;
	B.y = originalEnd.y + cur.y - startPoint.y;
}

int Object::CheckMouseIsResizing(Point & cur)
{
	int top = (A.y < B.y ? A.y : B.y);
	int left = (A.x < B.x ? A.x : B.x);
	int bot = (A.y > B.y ? A.y : B.y);
	int right = (A.x > B.x ? A.x : B.x);
	//Xet trong khoang chap nhan
	if (cur.y < top - 4 || cur.y > bot + 4 || cur.x < left - 4 || cur.x > right + 4)
		return 0;
	//Xet 8 huong
	if (cur.y >= top - 4 && cur.y <= top + 3 && cur.x >= left - 4 && cur.x <= left + 3)   // phia tren ben trai
		return 1;
	else if (cur.x >= right - 4 && cur.x <= right + 4 && cur.y >= bot - 4 && cur.y <= bot + 4)  //duoi cung ben phai
		return 2;
	else if (cur.x >= left - 4 && cur.x <= left + 3 && cur.y >= bot - 3 && cur.y <= bot + 4)  // duoi cung ben trai
		return 4;
	else if (cur.x >= right - 4 && cur.x <= right + 4 && cur.y >= top - 4 && cur.y <= top + 4) // phia tren ben phai
		return 3;
	else if (cur.x >= left - 4 && cur.x <= left + 3 && cur.y >= (bot + top) / 2 - 3 && cur.y <= (bot + top) / 2 + 3)  // trai
		return 5;
	else if (cur.x >= right - 4 && cur.x <= right + 4 && cur.y >= (bot + top) / 2 - 3 && cur.y <= (bot + top) / 2 + 3)  // phai
		return 6;
	else if (cur.x >= (right + left) / 2 - 3 && cur.x <= (right + left) / 2 + 3 && cur.y >= top - 4 && cur.y <= top + 3) // tren
		return 7;
	else if (cur.x >= (right + left) / 2 - 3 && cur.x <= (right + left) / 2 + 3 && cur.y >= bot - 3 && cur.y <= bot + 4) // duoi
		return 8;
	return 0;

}

void Object::ResizingObj(Point & cur, int mode)
{
	int tmpX, tmpY;
	tmpX = cur.x - startPoint.x;
	tmpY = cur.y - startPoint.y;
	Point beginP, endP;
	beginP.x = beginP.y = endP.x = endP.y = 0;
	if (mode == 1) {
		beginP.x = tmpX;
		beginP.y = tmpY;
	}
	else if (mode == 2) {
		endP.x = tmpX;
		endP.y = tmpY;
	}
	else if (mode == 3) {
		endP.x = tmpX;
		beginP.y = tmpY;
	}
	else if (mode == 4) {
		beginP.x = tmpX;
		endP.y = tmpY;
	}
	else if (mode == 5) {
		beginP.x = tmpX;
	}
	else if (mode == 6) {
		endP.x = tmpX;
	}
	else if (mode == 7) {
		beginP.y = tmpY;
	}
	else if (mode == 8) {
		endP.y = tmpY;
	}
	if (originalBegin.x < originalEnd.x && originalBegin.y < originalEnd.y)  
	{
		A.x = originalBegin.x + beginP.x;
		A.y = originalBegin.y + beginP.y;
		B.x = originalEnd.x + endP.x;
		B.y = originalEnd.y + endP.y;
	}
	else if (originalBegin.x < originalEnd.x && originalBegin.y >= originalEnd.y) 
	{
		A.x = originalBegin.x + beginP.x;
		A.y = originalBegin.y + endP.y;
		B.x = originalEnd.x + endP.x;
		B.y = originalEnd.y + beginP.y;
	}
	else if (originalBegin.x >= originalEnd.x && originalBegin.y < originalEnd.y) 
	{
		A.x = originalBegin.x + endP.x;
		A.y = originalBegin.y + beginP.y;
		B.x = originalEnd.x + beginP.x;
		B.y = originalEnd.y + endP.y;
	}
	else
	{
		A.x = originalBegin.x + endP.x;
		A.y = originalBegin.y + endP.y;
		B.x = originalEnd.x + beginP.x;
		B.y = originalEnd.y + beginP.y;
	}
}

Line::Line() :Object() {}
void Line::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 3, color);
	HBRUSH hbrush = CreateSolidBrush(color);
	SelectObject(hdc, hPen);
	SelectObject(hdc, hbrush);
	MoveToEx(hdc, A.x, A.y, NULL);
	LineTo(hdc, B.x, B.y);
	DeleteObject(hPen);
	DeleteObject(hbrush);
}

bool Line::PointInObj(HDC hdc, Point p)
{
	if (A.x == B.x && A.y == B.y)
		return false;
	if (A.y <= B.y && A.x <=B.x) {
		if (p.y <A.y || p.y > B.y || p.x < A.x || p.x > B.x)
			return false;
	}
	else if (A.y >= B.y && A.x <B.x) {
		if (p.y > A.y || p.y < B.y || p.x < A.x || p.x > B.x)
			return false;
	}
	else if (A.y > B.y && A.x > B.x) {
		if (p.y > A.y || p.y < B.y || p.x >A.x || p.x < B.x)
			return false;
	}
	else if (A.y <B.y && A.x > B.x) {
		if (p.y < A.y || p.y >B.y || p.x > A.x || p.x < B.x)
			return false;
	}
	if (p.x == A.x && p.y == A.y || p.x == B.x && p.y == B.y)
		return true;
	if ((p.x - A.x) == 0 || (p.x - B.x) == 0)
		return true;
	if ((p.y - A.y) / (p.x - A.x) == (p.y - B.y) / (p.x - B.x) || (p.y - A.y) / (p.x - A.x) == -(p.y - B.y) / (p.x - B.x))
		return true;
	return false;
}

void Line::SelectRect(HDC hdc)
{
	HPEN hPen = CreatePen(PS_DASHDOT, 1, RGB(0, 0, 0));
	HGDIOBJ oldBrush = SelectObject(hdc, GetStockObject(NULL_BRUSH));
	SelectObject(hdc, hPen);
	Rectangle(hdc, A.x, A.y, B.x, B.y);
	DeleteObject(hPen);

	int top = (A.y < B.y ? A.y : B.y) ;
	int left = (A.x < B.x ? A.x : B.x);
	int bot = (A.y > B.y ? A.y : B.y) ;
	int right = (A.x > B.x ? A.x : B.x);
	Rectangle(hdc, left, top, right, bot);
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
	Rectangle(hdc, left - 4, top - 4, left + 1, top + 1);
	Rectangle(hdc, right - 1, top - 4, right + 4, top + 1);
	Rectangle(hdc, left - 4, bot - 1, left + 1, bot + 4);
	Rectangle(hdc, right - 1, bot - 1, right + 4, bot + 4);
	Rectangle(hdc, left - 4, (bot + top) / 2 - 3, left + 1, (bot + top) / 2 + 2);
	Rectangle(hdc, right - 1, (bot + top) / 2 - 3, right + 4, (bot + top) / 2 + 2);
	Rectangle(hdc, (right + left) / 2 - 3, top - 4, (right + left) / 2 + 2, top + 1);
	Rectangle(hdc, (right + left) / 2 - 3, bot - 1, (right + left) / 2 + 2, bot + 4);
	SelectObject(hdc, oldBrush);
	DeleteObject(oldBrush);
}

Rectangles::Rectangles() :Object() {}
void Rectangles::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 3, color);
	HBRUSH hbrush = CreateSolidBrush(color);
	SelectObject(hdc, hPen);
	SelectObject(hdc, hbrush);
	Rectangle(hdc, A.x, A.y, B.x, B.y);
	DeleteObject(hPen);
	DeleteObject(hbrush);
}

bool Rectangles::PointInObj(HDC hdc, Point p)
{
	if ((p.x >= A.x-1 && p.x <= B.x+1 && p.y <= B.y+1 && p.y >= A.y-1) || (p.x <= A.x+1 && p.x >= B.x-1 && p.y >= B.y-1 && p.y <= A.y+1))
		return true;
	return false;
}

void Rectangles::SelectRect(HDC hdc)
{
	HPEN hPen;
	if(color == RGB(0, 0, 0))
		hPen = CreatePen(PS_DOT, 1, RGB(255,255, 255));
	else
		hPen = CreatePen(PS_DOT, 1, RGB(0, 0, 0));
	HGDIOBJ oldBrush = SelectObject(hdc, GetStockObject(NULL_BRUSH));
	SelectObject(hdc, hPen);
	Rectangle(hdc, A.x, A.y, B.x, B.y);
	DeleteObject(hPen);
	int top = (A.y < B.y ? A.y : B.y);
	int left = (A.x < B.x ? A.x : B.x);
	int bot = (A.y > B.y ? A.y : B.y);
	int right = (A.x > B.x ? A.x : B.x);
	Rectangle(hdc, left, top, right, bot);
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
	Rectangle(hdc, left - 4, top - 4, left + 1, top + 1);
	Rectangle(hdc, right - 4, top - 4, right + 4, top + 1);
	Rectangle(hdc, left - 4, bot - 1, left + 1, bot + 4);
	Rectangle(hdc, right - 1, bot - 1, right + 4, bot + 4);
	Rectangle(hdc, left - 4, (bot + top) / 2 - 3, left + 1, (bot + top) / 2 + 2);
	Rectangle(hdc, right - 1, (bot + top) / 2 - 3, right + 4, (bot + top) / 2 + 2);
	Rectangle(hdc, (right + left) / 2 - 3, top - 4, (right + left) / 2 + 2, top + 1);
	Rectangle(hdc, (right + left) / 2 - 3, bot - 1, (right + left) / 2 + 2, bot + 4);
	SelectObject(hdc, oldBrush);
	DeleteObject(oldBrush);
}

Ellipses::Ellipses() : Object() {}
void Ellipses::Draw(HDC hdc)
{
	HPEN hPen = CreatePen(PS_SOLID, 3, color);
	HBRUSH hbrush = CreateSolidBrush(color);
	SelectObject(hdc, hPen);
	SelectObject(hdc, hbrush);
	Ellipse(hdc, A.x, A.y, B.x, B.y);
	DeleteObject(hPen);
	DeleteObject(hbrush);
}

bool Ellipses::PointInObj(HDC hdc, Point p)
{
	int top  = (A.y < B.y ? A.y : B.y);
	int left = (A.x < B.x ? A.x : B.x);
	int bot = (A.y > B.y ? A.y : B.y);
	int right = (A.x > B.x ? A.x : B.x);
	if (p.y<top || p.y >bot || p.x<left || p.x>right)
		return false;
	float a = (float)(right - left) / 2.0;
	float b = (float)(bot - top) / 2.0;
	float h = (float)(right + left) / 2.0;
	float k = (float)(bot + top) / 2.0;
	float result = pow(p.x - h, 2) / pow(a, 2) + pow(p.y - k, 2) / pow(b, 2);
	float delta = 0.1;
	if (result <= 1)
		return true;
	else if (result <= 1 + delta && result >= 1 - delta)
		return false;
}

void Ellipses::SelectRect(HDC hdc)
{
	HPEN hPen = CreatePen(PS_DASHDOT, 1, RGB(0, 0, 0));
	HGDIOBJ oldBrush = SelectObject(hdc, GetStockObject(NULL_BRUSH));
	SelectObject(hdc, hPen);
	Rectangle(hdc, A.x, A.y, B.x, B.y);
	DeleteObject(hPen);
	int top = (A.y < B.y ? A.y : B.y);
	int left = (A.x < B.x ? A.x : B.x);
	int bot = (A.y > B.y ? A.y : B.y);
	int right = (A.x > B.x ? A.x : B.x);
	Rectangle(hdc, left, top, right, bot);
	SelectObject(hdc, GetStockObject(WHITE_BRUSH));
	Rectangle(hdc, left - 4, top - 4, left + 1, top + 1);
	Rectangle(hdc, right - 1, top - 4, right + 4, top + 1);
	Rectangle(hdc, left - 4, bot - 1, left + 1, bot + 4);
	Rectangle(hdc, right - 1, bot - 1, right + 4, bot + 4);
	Rectangle(hdc, left - 4, (bot + top) / 2 - 3, left + 1, (bot + top) / 2 + 2);
	Rectangle(hdc, right - 1, (bot + top) / 2 - 3, right + 4, (bot + top) / 2 + 2);
	Rectangle(hdc, (right + left) / 2 - 3, top - 4, (right + left) / 2 + 2, top + 1);
	Rectangle(hdc, (right + left) / 2 - 3, bot - 1, (right + left) / 2 + 2, bot + 4);
	SelectObject(hdc, oldBrush);
	DeleteObject(oldBrush);
}

Texts::Texts() : Object() {}

void Texts::Draw(HDC hdc)
{
	HFONT hFont = CreateFontIndirect(&font);
	lfHeight = font.lfHeight;
	SelectObject(hdc, hFont);
	SetTextColor(hdc, color);
	TextOut(hdc, A.x, A.y, str, wcsnlen(str, MAX_TEXT_LENGTH));
	DeleteObject(hFont);
}

bool Texts::PointInObj(HDC hdc, Point p)
{
	//(p.x >= A.x - 1 && p.x <= B.x + 1 && p.y <= B.y + 1 && p.y >= A.y - 1)
	if (p.x >= A.x-1 && p.x <= A.x+ wcsnlen_s(str, MAX_TEXT_LENGTH)*10 && p.y >= A.y-1 && p.y <= A.y+30)
		return true;
	return false;
}

void Texts::SelectRect(HDC hdc)
{
	HPEN hPen = CreatePen(PS_DASHDOT, 1, RGB(0, 0, 0));
	HGDIOBJ oldBrush = SelectObject(hdc, GetStockObject(NULL_BRUSH));
	SelectObject(hdc, hPen);
	HFONT hFont = CreateFontIndirect(&font);
	SelectObject(hdc, hFont);
	SIZE size;
	GetTextExtentPoint32(hdc, str, wcsnlen(str, MAX_TEXT_LENGTH), &size);
	Rectangle(hdc, A.x, A.y, A.x + size.cx, A.y +size.cy);
	DeleteObject(hPen);
}
