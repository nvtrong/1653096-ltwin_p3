// MyPaint.cpp : Defines the entry point for the application.
//

//em muon source p2 cua ban de hoan thanh P3
//Source p2: Nguyen Van Tu 

#include "stdafx.h"
#include "MyPaint.h"
#include "Object.h"
#include <commctrl.h>	
#define MAX_LOADSTRING 100	

#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32
#define MAX_OBJECT	50
static int typeMSG;
static bool checkOpenFile=0;
WCHAR str[MAX_TEXT_LENGTH];
static HWND hwndActive;
static Object *object = NULL;
static int CheckSize = 0;
static bool Check = false; // select object
 
bool stupidText = false;

WCHAR szMyFriendLine[20] = L"MyFriendLine";
WCHAR szMyFriendRect[20] = L"MyFriendRect";
WCHAR szMyFriendElip[20] = L"MyFriendElip";
WCHAR szMyFriendText[20] = L"MyFriendText";

class CHILD_WND_DATA {
public:
	HWND hWnd;
	COLORREF color;
	LOGFONT font;
	Object * obj[MAX_OBJECT];
	int n;
	bool isSaved;
	//wchar_t *str[MAX_TEXT_LENGTH];
};
static CHILD_WND_DATA *tmp = new CHILD_WND_DATA;
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
WCHAR szDrawTitle[MAX_LOADSTRING];				// caption of Edit Draw window
WCHAR szDrawWindowClass[MAX_LOADSTRING];		// the draw window class name
HWND hFrameWnd;
HWND hwndMDIClient = NULL;						//handle of client window
HWND hToolBarWnd;	//handle of tool bar
int nCurrentMenuSelectItem, newMenuItem;
int count_Draw = 0;


//MyFriend Function Begin----------------------------------------------------------------------
void OnLButtondown(HWND HWnd, WPARAM wParam, LPARAM lParam, int type, Point&p1, Point&p2);
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int type, Point&p1, Point&p2);
void OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam, int type, Point p1, Point&p2);
void OnchildPaint(HWND hWnd);
void OnSelectObject(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnMovingObject(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnSizingObject(HWND hWnd, WPARAM wParam, LPARAM lParam);
COLORREF rgbCurrentColor = RGB(0, 0, 0);		//default color is black
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	DrawWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	MDICloseProc(HWND, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	TextDialog(HWND, UINT, WPARAM, LPARAM);
void InitChildWindow(HWND hWnd);
void OnMDIActivate(HWND hWnd);
void onNewDrawTextWnd(HWND hWnd);
void UpdateMenuState(HWND hwnd, int newMenuItem);
void chooseColorDialog(HWND hWnd/*, COLORREF &color*/);
void chooseFontDialog(HWND hWnd);
void doToolBar(HWND hWnd);
void saveFileAs(HWND hWnd);
void openFile(HWND hWnd);
void onNewDrawFromOpenF(HWND hWnd, TCHAR fileName[256]);
void initChildWindowFromOpenF(HWND hWnd, CHILD_WND_DATA *&tmp);

//MyFriend Function End----------------------------------------------------------------------

//MyFunction
void onCopy(HWND hWnd);
void onCut(HWND hWnd);
void onPast(HWND hWnd);
void onDelete(HWND hWnd);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYPAINT, szWindowClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDS_APP_TITLE_DRAW, szDrawTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_DRAW, szDrawWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (!TranslateMDISysAccel(hwndMDIClient, &msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYPAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if (!RegisterClassExW(&wcex)) return FALSE;
    
	//child window class: Draw
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8; // extra 8 byte(64 bit) to store data poiter of child window
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szDrawWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if (!RegisterClassExW(&wcex)) return FALSE;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}
void InitFrameWindow(HWND hWnd)
{
	hFrameWnd = hWnd;
	//create MDI client window.
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2); //position of window popup
	ccs.idFirstChild = 5000; // first id of menu item to be added.
	hwndMDIClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN, 0, 0, 0, 0, hWnd, (HMENU)NULL, hInst, (LPVOID)&ccs);
	ShowWindow(hwndMDIClient, SW_SHOW);
	HMENU hMenu = GetMenu(hFrameWnd);
	EnableMenuItem(hMenu, 1, MF_GRAYED | MF_BYPOSITION);
	HMENU hPopUp = GetSubMenu(GetMenu(hWnd), 1);
	CheckMenuItem(hPopUp, IDM_LINE, MF_CHECKED | MF_BYCOMMAND);
	doToolBar(hWnd);
}
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND childwind;
    switch (message)
    {
		
	case WM_CREATE:
		InitFrameWindow(hWnd);
		return 0;
    case WM_COMMAND:
        {
		wchar_t c[50];
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;  
			case IDM_NEW:
				onNewDrawTextWnd(hWnd);
				RECT r;
				GetClientRect(hwndActive, &r);
				InvalidateRect(hwndActive, &r, 1);
				break;
			case IDM_OPEN:
				openFile(hWnd);
				break;
			case IDM_SAVE:
				childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
				saveFileAs(childwind);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case IDM_COLOR:
				childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
				chooseColorDialog(childwind);
				break;
			case IDM_FONT:
				childwind = (HWND)SendMessage(hwndMDIClient, WM_MDIGETACTIVE, 0, 0);
				chooseFontDialog(childwind);
				break;
			case IDM_LINE:
				if (wmId == IDM_LINE)
					typeMSG = 0;
			case IDM_RECTANGLE:
				if (wmId == IDM_RECTANGLE)
					typeMSG = 1;
			case IDM_ELLIPSE:
				if (wmId == IDM_ELLIPSE)
					typeMSG = 2;
			case IDM_TEXT:
				if (wmId == IDM_TEXT)
					typeMSG = 3;
			case IDM_SELECTOBJECT:
				newMenuItem = LOWORD(wParam);
				UpdateMenuState(hWnd, newMenuItem);
				if (wmId == IDM_SELECTOBJECT)
					typeMSG = 4;
				break;
			case IDM_TIDE:
				SendMessage(hwndMDIClient, WM_MDITILE, MDITILE_VERTICAL, 0);
				break;
			case IDM_CASCADE:
				SendMessage(hwndMDIClient, WM_MDICASCADE, 0, 0);
				break;
			case IDM_CLOSEALL:
				EnumChildWindows(hwndMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
				count_Draw = 0;
				break;
			case IDM_EDITCOPY:
				onCopy(hWnd);
				break;
			case IDM_EDITCUT:
				onCut(hWnd);
				break;
			case IDM_EDITPASTE:
				onPast(hWnd);
				break;
			case IDM_EDITDELETE:
				onDelete(hWnd);
				break;

            /*default:
                return DefWindowProc(hWnd, message, wParam, lParam);*/
            }
        }
		break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
		return 0;
    case WM_DESTROY:
        PostQuitMessage(0);
		return 0;
	case WM_SIZE:
		UINT w, h;
		//Update the size of MDI client window
		w = LOWORD(lParam);
		h = HIWORD(lParam);
		//if (wParam == SIZE_MAXIMIZED)
		MoveWindow(hwndMDIClient, 0, 28, w, h-28, TRUE);
		//return DefWindowProc(hwndMDIClient, message, wParam, lParam);
		return 0;
    }
	return DefFrameProc(hWnd, hwndMDIClient, message, wParam, lParam);
}
LRESULT CALLBACK DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Point p1, p2;
	switch (message)
	{
	case WM_CREATE:
		if (checkOpenFile == 1) {
			initChildWindowFromOpenF(hWnd, tmp);
			InvalidateRect(hWnd, NULL, true);
			checkOpenFile = 0;
		}
		else
			InitChildWindow(hWnd);
		return 0;
	case WM_DESTROY:
		return 0;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		count_Draw--;
		break;
	case WM_SIZE:
		break;
	case WM_MDIACTIVATE:
		OnMDIActivate(hWnd);
		break;
	case WM_LBUTTONDOWN:
	{
		if (typeMSG == 3)
			OnLButtondown(hWnd, wParam, lParam, 3, p1, p2);
		else if (typeMSG == 4) {
			OnSelectObject(hWnd, wParam, lParam);
		}
		else
			OnLButtondown(hWnd, wParam, lParam, 0, p1, p2);
	}
	break;
	case WM_LBUTTONUP:
	{
		Check = false;
		CheckSize = 0;
		if (typeMSG == 0)
			OnLButtonUp(hWnd, wParam, lParam, 0, p1, p2);
		else if (typeMSG == 1)
			OnLButtonUp(hWnd, wParam, lParam, 1, p1, p2);
		else if (typeMSG == 2)
			OnLButtonUp(hWnd, wParam, lParam, 2, p1, p2);

	}
	break;
	case WM_MOUSEMOVE:
	{
		if (typeMSG == 0)
			OnMouseMove(hWnd, wParam, lParam, 0, p1, p2);
		else if (typeMSG == 1)
			OnMouseMove(hWnd, wParam, lParam, 1, p1, p2);
		else if (typeMSG == 2)
			OnMouseMove(hWnd, wParam, lParam, 2, p1, p2);
		else if (typeMSG == 4 && Check == true) {
			OnMovingObject(hWnd, wParam, lParam);
		}
		else if (typeMSG == 4 && CheckSize != 0) {
			OnSizingObject(hWnd, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
		OnchildPaint(hWnd);
	break;
	}
	return DefMDIChildProc(hWnd, message, wParam, lParam);
}
// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hwndMDIClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L);
	return 1;
}
void InitChildWindow(HWND hWnd)
{
	//init data structure for child window
	CHILD_WND_DATA * wndData;
	wndData = (CHILD_WND_DATA*) VirtualAlloc(NULL, sizeof(CHILD_WND_DATA), MEM_COMMIT, PAGE_READWRITE);
	wndData->hWnd = hWnd;
	wndData->color = RGB(0, 0, 0);
	ZeroMemory(&(wndData->font), sizeof(LOGFONT));
	wndData->isSaved = false;
	wndData->font.lfHeight = 20;
	wcscpy_s(wndData->font.lfFaceName, LF_FACESIZE, L"Arial");
	wndData->n = 0;
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData) == 0)
		if (GetLastError() != 0) {
			int a = 0;
		}
	HMENU hMenu = GetMenu(hFrameWnd);
	EnableMenuItem(hMenu, 1, MF_ENABLED | MF_BYPOSITION);
}
void OnMDIActivate(HWND hWnd)
{
	hwndActive = hWnd;
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL) return;
	HMENU hMenu = GetMenu(hFrameWnd);
	DrawMenuBar(hFrameWnd);
}
void onNewDrawTextWnd(HWND hWnd)
{
	count_Draw++;
	wsprintf(szDrawTitle, TEXT("%s %d .drw"), L"Noname-", count_Draw);
	//data structure to create a MDI child window
	MDICREATESTRUCT mdiCreate;
	ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));
	mdiCreate.szClass = szDrawWindowClass; 
	mdiCreate.szTitle = szDrawTitle;	  
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	//this is an "open" parameter, usually a pointer which contains private data
	mdiCreate.lParam = NULL;
	//send message to MDI client window to create a child window

	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
}
void UpdateMenuState(HWND hwnd, int newMenuItem)
{
	HMENU hPopUp = GetMenu(hwnd);
	CheckMenuItem(hPopUp, IDM_LINE, MF_UNCHECKED | MF_BYCOMMAND);
	 //hPopUp = GetSubMenu(GetMenu(hwnd), 1);//Get handle of draw popup
	CheckMenuItem(hPopUp, nCurrentMenuSelectItem, MF_UNCHECKED | MF_BYCOMMAND); // uncheck current selected item
	CheckMenuItem(hPopUp, newMenuItem, MF_CHECKED | MF_BYCOMMAND);			   // check newitem
	nCurrentMenuSelectItem = newMenuItem;
}
void chooseColorDialog(HWND hWnd)
{
	CHOOSECOLOR cc;
	HDC hdc = GetDC(hWnd);
	COLORREF acrCustClr[16];
	DWORD rgbCurrent = RGB(0, 0, 0);
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	if (ChooseColor(&cc)) {
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
		if (wndData == NULL) return;
		wndData->color = cc.rgbResult;
	}
	ReleaseDC(hWnd, hdc);
}
void chooseFontDialog(HWND hWnd){
	CHOOSEFONT cf; 
	LOGFONT lf; 
	HFONT hfNew, hfOld;
	HDC hdc = GetDC(hWnd);
	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd; // handle của window cha
	cf.lpLogFont = &lf;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	if (ChooseFont(&cf)) {
		hfNew = CreateFontIndirect(cf.lpLogFont);
		hfOld = (HFONT)SelectObject(hdc, hfNew);
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
		if (wndData == NULL) return;
		wndData->font = lf;
	}
	ReleaseDC(hWnd, hdc);
}
void doToolBar(HWND hWnd)
{
	InitCommonControls(); //loading common control DLl
	TBBUTTON tbbutton[] = {
		{STD_FILENEW,  IDM_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0},
		{STD_FILEOPEN, IDM_OPEN,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0},
		{STD_FILESAVE, IDM_SAVE,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0}
	};
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbbutton) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbbutton,
		sizeof(tbbutton) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));

	TBBUTTON tbbutton1[] = {
		{ 0, 0, TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, IDM_LINE,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, IDM_RECTANGLE,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, IDM_ELLIPSE,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, IDM_TEXT,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 4, IDM_SELECTOBJECT,TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_CUT,	IDM_EDITCUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_COPY,	IDM_EDITCOPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_PASTE, IDM_EDITPASTE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	{ STD_DELETE,IDM_EDITDELETE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	TBADDBITMAP	tbBitmap = { hInst, IDB_DRAW };

	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	// identify the bitmap index of each button
	tbbutton1[1].iBitmap += idx;
	tbbutton1[2].iBitmap += idx;
	tbbutton1[3].iBitmap += idx;
	tbbutton1[4].iBitmap += idx;
	tbbutton1[5].iBitmap += idx;

	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbbutton1) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbbutton1);

}
void OnLButtondown(HWND HWnd, WPARAM wParam, LPARAM lParam, int type, Point&p1, Point&p2)
{
	p1.x = p2.x = LOWORD(lParam);
	p1.y = p2.y = HIWORD(lParam);
	if (type == 3) {
		if (wParam && MK_LBUTTON == MK_LBUTTON) {
			HDC hdc = GetDC(HWnd);
			//HWND editBox = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER, p1.x, p1.y, p1.x + 5, p1.y + 5, HWnd, nullptr, hInst, nullptr);
			//ShowWindow(HWnd, SW_NORMAL)
			//ReleaseDC(HWnd,hdc);
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), HWnd, TextDialog);
			if (stupidText == false)
				return;
			CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(HWnd, 0);
			
			Texts *txt = new Texts;
			txt->A.x = p1.x;
			txt->A.y = p1.y;
			wcscpy_s(txt->str, str);
			txt->color = wndData->color;
			txt->font = wndData->font;
			txt->Draw(hdc);
			wndData->obj[wndData->n] = (Texts *)txt;
			wndData->obj[wndData->n]->type = 3;
			wndData->obj[wndData->n]->font = txt->font;
			wcscpy_s(wndData->obj[wndData->n]->str, str);
			wndData->obj[wndData->n]->count_str = wcsnlen(str, MAX_TEXT_LENGTH);
			wndData->obj[wndData->n]->color = txt->color;
			wndData->obj[wndData->n]->font = wndData->font;
			wndData->n++;
			ReleaseDC(HWnd, hdc);
		}
	}
}
void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam, int type, Point&p1, Point&p2)
{
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	if (type == 0) {
		if (wParam && MK_LBUTTON == MK_LBUTTON) {
			HDC hdc = GetDC(hWnd);
			Line * li = new Line;
			li->A.x = p1.x;
			li->A.y = p1.y;
			li->B.x = p2.x;
			li->B.y = p2.y;
			li->color = wndData->color;
			SetROP2(hdc, R2_NOTXORPEN);
			li->Draw(hdc);
			p2.x = LOWORD(lParam);
			p2.y = HIWORD(lParam);
			li->B.x = p2.x;
			li->B.y = p2.y;
			li->Draw(hdc);
			ReleaseDC(hWnd, hdc);//new
		}
	}
	else if (type == 1) {
		if (wParam && MK_LBUTTON == MK_LBUTTON) {
			HDC hdc = GetDC(hWnd);
			Rectangles * rtg = new Rectangles;
			rtg->A.x = p1.x;
			rtg->A.y = p1.y;
			rtg->B.x = p2.x;
			rtg->B.y = p2.y;
			rtg->color = wndData->color;
			SetROP2(hdc, R2_NOTXORPEN);
			rtg->Draw(hdc);
			p2.x = LOWORD(lParam);
			p2.y = HIWORD(lParam);
			rtg->B.x = p2.x;
			rtg->B.y = p2.y;
			rtg->Draw(hdc);
			ReleaseDC(hWnd, hdc);
		}
	}
	else if (type == 2) {
		if (wParam && MK_LBUTTON == MK_LBUTTON) {
			HDC hdc = GetDC(hWnd);
			Ellipses * els = new Ellipses;
			els->A.x = p1.x;
			els->A.y = p1.y;
			els->B.x = p2.x;
			els->B.y = p2.y;
			els->color = wndData->color;
			SetROP2(hdc, R2_NOTXORPEN);
			els->Draw(hdc);
			p2.x = LOWORD(lParam);
			p2.y = HIWORD(lParam);
			els->B.x = p2.x;
			els->B.y = p2.y;
			els->Draw(hdc);
			ReleaseDC(hWnd, hdc);
		}
	}

	
}
void OnLButtonUp(HWND hWnd, WPARAM wParam, LPARAM lParam, int type, Point p1, Point&p2)
{
	p2.x = LOWORD(lParam);
	p2.y = HIWORD(lParam);
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL) return;
	if (type == 0) 
	{
		wndData->obj[wndData->n] = new Line;
		wndData->obj[wndData->n]->color = wndData->color;
		wndData->obj[wndData->n]->A.x = p1.x;
		wndData->obj[wndData->n]->A.y = p1.y;
		wndData->obj[wndData->n]->B.x = p2.x;
		wndData->obj[wndData->n]->B.y = p2.y;
		wndData->hWnd = hWnd;
		wndData->obj[wndData->n]->type = 0;
		wndData->n++;
		HDC hdc = GetDC(hWnd);
		Line * li = new Line;
		li->A.x = p1.x;
		li->A.y = p1.y;
		li->B.x = p2.x;
		li->B.y = p2.y;
		li->color = wndData->color;
		li->Draw(hdc);
		ReleaseDC(hWnd, hdc);
	}
	else if (type == 1) 
	{
		wndData->obj[wndData->n] = new Rectangles;
		wndData->obj[wndData->n]->color= wndData->color;
		wndData->obj[wndData->n]->A.x = p1.x;
		wndData->obj[wndData->n]->A.y = p1.y;
		wndData->obj[wndData->n]->B.x = p2.x;
		wndData->obj[wndData->n]->B.y = p2.y;
		wndData->hWnd = hWnd;
		wndData->obj[wndData->n]->type = 1;
		wndData->n++;
		HDC hdc = GetDC(hWnd);
		Rectangles * rtg = new Rectangles;
		rtg->A.x = p1.x;
		rtg->A.y = p1.y;
		rtg->B.x = p2.x;
		rtg->B.y = p2.y;
		rtg->color = wndData->color;
		rtg->Draw(hdc);
		ReleaseDC(hWnd, hdc);
	}
	if (type == 2) 
	{
		wndData->obj[wndData->n] = new Ellipses;
		wndData->obj[wndData->n]->color = wndData->color;
		wndData->obj[wndData->n]->A.x = p1.x;
		wndData->obj[wndData->n]->A.y = p1.y;
		wndData->obj[wndData->n]->B.x = p2.x;
		wndData->obj[wndData->n]->B.y = p2.y;
		wndData->hWnd = hWnd;
		wndData->obj[wndData->n]->type = 2;
		wndData->n++;
		HDC hdc = GetDC(hWnd);
		Ellipses * els = new Ellipses;
		els->A.x = p1.x;
		els->A.y = p1.y;
		els->B.x = p2.x;
		els->B.y = p2.y;
		els->color = wndData->color;
		els->Draw(hdc);
		ReleaseDC(hWnd, hdc);
	}
}
void OnchildPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	if (wndData == NULL) return;

	for (int i = 0; i < wndData->n; i++) {
			//wndData->obj[i]->color = wndData->color;
			//wndData->obj[i]->font = wndData->font;
			wndData->obj[i]->Draw(hdc);
	}	
	EndPaint(hWnd, &ps);
}
void OnSelectObject(HWND hWnd, WPARAM wParam, LPARAM lParam)
{

	static Point p;
	p.x = LOWORD(lParam);
	p.y = HIWORD(lParam);
	HDC hdc = GetDC(hWnd);
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	for (int i = wndData->n-1; i >=0; i--) {
		if (wndData->obj[i]->PointInObj(hdc, p) == true) {
			wndData->obj[i]->SelectRect(hdc);
			object=wndData->obj[i];
			object->StartPoint(p);
			Check = true;
			CheckSize = object->CheckMouseIsResizing(p);
			break;
		}
	}	
	ReleaseDC(hWnd, hdc);
	if (CheckSize == 0)
		return;
	else
		Check = false;
}
void OnMovingObject(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	static Point p;
	HDC hdc = GetDC(hWnd);
	p.x = LOWORD(lParam);
	p.y = HIWORD(lParam);
	object->SelectRect(hdc);
	object->MovingObj(p);
	ReleaseDC(hWnd, hdc);
	InvalidateRect(hWnd, NULL, true);
}
void OnSizingObject(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	static Point p;
	p.x = LOWORD(lParam);
	p.y = HIWORD(lParam);
	object->ResizingObj(p, CheckSize);
	InvalidateRect(hWnd, NULL, true);
}
INT_PTR CALLBACK TextDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, IDC_INPUTTEXT));
		return (INT_PTR)FALSE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_INPUTTEXT:
			break;
		case IDC_OK:
			stupidText = true;
			GetDlgItemText(hDlg, IDC_INPUTTEXT, str, MAX_TEXT_LENGTH);
			EndDialog(hDlg, IDCLOSE);
			break;
		case IDCLOSE:
			GetDlgItemText(hDlg, IDC_INPUTTEXT, str, MAX_TEXT_LENGTH);
			EndDialog(hDlg, IDCLOSE);
			break;
		case IDM_EXIT:
			DestroyWindow(hDlg);
			break;
		}
		break;
	case WM_CLOSE:
		stupidText = false;
		EndDialog(hDlg, IDCLOSE);
		DestroyWindow(hDlg);
		break;
	default:
		return DefWindowProc(hDlg, message, wParam, lParam);
	}
	return (INT_PTR)FALSE;
}
void saveFileAs(HWND hWnd)
{
	OPENFILENAME ofn; //CTDL dialog save
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT( "Binary Files (*.drw)\0*.drw\0Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0");
	wsprintf(szFile, TEXT("%s"), szDrawTitle);
	ZeroMemory(&ofn, sizeof(OPENFILENAME));	
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;	//handle của window cha
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile; //Chuỗi tên file trả về
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	if (wndData->isSaved == false) {
		if (GetSaveFileName(&ofn)) {
			ofstream ofs;
			ofs.open(szFile, ios::binary);
			wsprintf(szDrawTitle, TEXT("%s"), szFile);
			SetWindowText(hWnd, szDrawTitle);

			ofs.write((char*)&wndData->color, sizeof(COLORREF));
			ofs.write((char*)&wndData->font, sizeof(LOGFONT));
			ofs.write((char*)&wndData->n, sizeof(wndData->n));
			for (int i = 0; i < wndData->n; i++) {
				ofs.write((char*)&wndData->obj[i]->type, sizeof(wndData->obj[i]->type));
				if (wndData->obj[i]->type == 3) {
					ofs.write((char*)&wndData->obj[i]->count_str, sizeof(int));
					ofs.write((char*)&wndData->obj[i]->str, sizeof(wndData->obj[i]->str));
					ofs.write((char*)&wndData->obj[i]->A.x, sizeof(wndData->obj[i]->A.x));
					ofs.write((char*)&wndData->obj[i]->A.y, sizeof(wndData->obj[i]->A.y));
				}
				else {
					ofs.write((char*)&wndData->obj[i]->A.x, sizeof(wndData->obj[i]->A.x));
					ofs.write((char*)&wndData->obj[i]->A.y, sizeof(wndData->obj[i]->A.y));
					ofs.write((char*)&wndData->obj[i]->B.x, sizeof(wndData->obj[i]->B.x));
					ofs.write((char*)&wndData->obj[i]->B.y, sizeof(wndData->obj[i]->B.y));
				}
			}
			wndData->isSaved = true;
			ofs.close();
			MessageBox(hWnd, L"Đã lưu File", L"Notice", 0);
		}
		else
			MessageBox(hWnd, L"Bạn chưa lưu file", L"Lỗi", 0);
	}
	else
	{
		ofstream ofs;
		ofs.open(szFile, ios::binary);
		wsprintf(szDrawTitle, TEXT("%s"), szFile);
		SetWindowText(hWnd, szDrawTitle);

		ofs.write((char*)&wndData->color, sizeof(COLORREF));
		ofs.write((char*)&wndData->font, sizeof(LOGFONT));
		ofs.write((char*)&wndData->n, sizeof(wndData->n));
		for (int i = 0; i < wndData->n; i++) {
			ofs.write((char*)&wndData->obj[i]->type, sizeof(wndData->obj[i]->type));
			if (wndData->obj[i]->type == 3) {
				ofs.write((char*)&wndData->obj[i]->count_str, sizeof(int));
				ofs.write((char*)&wndData->obj[i]->str, sizeof(wndData->obj[i]->str));
				ofs.write((char*)&wndData->obj[i]->A.x, sizeof(wndData->obj[i]->A.x));
				ofs.write((char*)&wndData->obj[i]->A.y, sizeof(wndData->obj[i]->A.y));
			}
			else {
				ofs.write((char*)&wndData->obj[i]->A.x, sizeof(wndData->obj[i]->A.x));
				ofs.write((char*)&wndData->obj[i]->A.y, sizeof(wndData->obj[i]->A.y));
				ofs.write((char*)&wndData->obj[i]->B.x, sizeof(wndData->obj[i]->B.x));
				ofs.write((char*)&wndData->obj[i]->B.y, sizeof(wndData->obj[i]->B.y));
			}
		}

		ofs.close();
		MessageBox(hWnd, L"Đã lưu File", L"Notice", 0);
	}
}
void openFile(HWND hWnd)
{
	OPENFILENAME ofn; //CTDL dialog save
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("Binary Files (*.drw)\0*.drw\0Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0");
	wsprintf(szFile, TEXT("%s"), L"*.drw");
	//szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;	//handle của window cha
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile; //Chuỗi tên file trả về
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn)) {
		ifstream ifs;
		ifs.open(szFile, ios::binary);
		ifs.read((char*)&tmp->color, sizeof(COLORREF));
		ifs.read((char*)&tmp->font, sizeof(LOGFONT));
		ifs.read((char*)&tmp->n, sizeof(tmp->n));
		int type;
		for (int i = 0; i < tmp->n; i++) {
			ifs.read((char*)&type, sizeof(type));
			if (type == 0) {
				tmp->obj[i] = new Line;
				tmp->obj[i]->type = 0;
			}
			else if (type == 1) {
				tmp->obj[i] = new Rectangles;
				tmp->obj[i]->type = 1;
			}
			else if (type == 2) {
				tmp->obj[i] = new Ellipses;
				tmp->obj[i]->type = 2;
			}
			else if (type == 3) {
				tmp->obj[i] = new Texts;
				tmp->obj[i]->type = 3;
			}
			if (type == 3) {
				tmp->obj[i]->font = tmp->font;
				ifs.read((char*)&tmp->obj[i]->count_str, sizeof(int));
				ifs.read((char*)&tmp->obj[i]->str, sizeof(tmp->obj[i]->str));
				ifs.read((char*)&tmp->obj[i]->A.x, sizeof(tmp->obj[i]->A.x));
				ifs.read((char*)&tmp->obj[i]->A.y, sizeof(tmp->obj[i]->A.y));
			}
			else {
				ifs.read((char*)&tmp->obj[i]->A.x, sizeof(tmp->obj[i]->A.x));
				ifs.read((char*)&tmp->obj[i]->A.y, sizeof(tmp->obj[i]->A.y));
				ifs.read((char*)&tmp->obj[i]->B.x, sizeof(tmp->obj[i]->B.x));
				ifs.read((char*)&tmp->obj[i]->B.y, sizeof(tmp->obj[i]->B.y));
			}
		}
		ifs.close();
		checkOpenFile = 1;
		onNewDrawFromOpenF(hWnd, szFile);
	}
	else
		MessageBox(hWnd, L"Bạn chưa mở file", L"Lỗi", 0);
}

void initChildWindowFromOpenF(HWND hWnd, CHILD_WND_DATA *& tmp)
{
	CHILD_WND_DATA * wndData;
	wndData = (CHILD_WND_DATA*)VirtualAlloc(NULL, sizeof(CHILD_WND_DATA), MEM_COMMIT, PAGE_READWRITE);
	wndData = tmp;
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)wndData) == 0)
		if (GetLastError() != 0) {
			int a = 0;
		}
	HMENU hMenu = GetMenu(hFrameWnd);
	EnableMenuItem(hMenu, 1, MF_ENABLED | MF_BYPOSITION);
}

void onNewDrawFromOpenF(HWND hWnd, TCHAR fileName[256])
{
	MDICREATESTRUCT mdiCreate;
	ZeroMemory(&mdiCreate, sizeof(MDICREATESTRUCT));
	mdiCreate.szClass = szDrawWindowClass;
	mdiCreate.szTitle = fileName;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hwndMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
}


void onCopy(HWND hWnd) {
	


	//CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hWnd, 0);
	//if (wndData == NULL) return;
	HGLOBAL hgbmain = NULL;
	switch (object->type)
	{
	case 0:
	{
		int id = RegisterClipboardFormat(szMyFriendLine);
		hgbmain = GlobalAlloc(GHND, sizeof(Line));
		Line* li = (Line*)GlobalLock(hgbmain);
		
		*li =*(Line*) object;
		GlobalUnlock(hgbmain);
		if (OpenClipboard(hWnd)) {
			EmptyClipboard();
			SetClipboardData(id, hgbmain);
			CloseClipboard();
		}
	}
	break;
	case 1:
	{
		int id = RegisterClipboardFormat(szMyFriendRect);
		hgbmain = GlobalAlloc(GHND, sizeof(Rectangles));
		Rectangles* li = (Rectangles*)GlobalLock(hgbmain);

		*li = *(Rectangles*)object;
		GlobalUnlock(hgbmain);
		if (OpenClipboard(hWnd)) {
			EmptyClipboard();
			SetClipboardData(id, hgbmain);
			CloseClipboard();
		}
	}
	break;
	case 2:
	{
		int id = RegisterClipboardFormat(szMyFriendElip);
		hgbmain = GlobalAlloc(GHND, sizeof(Ellipses));
		Ellipses* li = (Ellipses*)GlobalLock(hgbmain);

		*li = *(Ellipses*)object;
		GlobalUnlock(hgbmain);
		if (OpenClipboard(hWnd)) {
			EmptyClipboard();
			SetClipboardData(id, hgbmain);
			CloseClipboard();
		}
	}
	break;
	case 3:
	{
		int id = RegisterClipboardFormat(szMyFriendText);
		hgbmain = GlobalAlloc(GHND, sizeof(Texts));
		Texts* li = (Texts*)GlobalLock(hgbmain);

		*li = *(Texts*)object;
		GlobalUnlock(hgbmain);
		if (OpenClipboard(hWnd)) {
			EmptyClipboard();
			SetClipboardData(id, hgbmain);
			CloseClipboard();
		}
	}
	break;
	default:
		break;
	}

}

void onCut(HWND hWnd)
{
	onCopy(hWnd);
	object->A.x = -1;
	object->A.y = -1;
	object->B.x = -1;
	object->B.y = -1;
	RECT r;
	GetClientRect(hwndActive, &r);
	InvalidateRect(hwndActive, &r, 1);
}

void onPast(HWND hWnd)
{
	if (!OpenClipboard(hWnd))
	{
		MessageBox(hWnd, _T("Open clipboard fail"), _T("Notice"), 0);
		return;
	}
	HANDLE hCopy;
	int idLine = RegisterClipboardFormat(szMyFriendLine);
	int idRect = RegisterClipboardFormat(szMyFriendRect);
	int idElip = RegisterClipboardFormat(szMyFriendElip);
	int idText = RegisterClipboardFormat(szMyFriendText);

	//hCopy = GetClipboardData(idLine);
	if ((hCopy = GetClipboardData(idLine)) != NULL)
	{
		Line* lcop = (Line*)GlobalLock(hCopy);
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hwndActive, 0);
		HDC hdc = GetDC(hwndActive);
		Line * li = new Line;
		li->A.x = lcop->A.x;
		li->A.y = lcop->A.y;
		li->B.x = lcop->B.x;
		li->B.y = lcop->B.y;
		li->color = lcop->color;
		//li->color = wndData->color;
		SetROP2(hdc, R2_NOTXORPEN);
		li->Draw(hdc);
		li->Draw(hdc);
		ReleaseDC(hwndActive, hdc);
		wndData->obj[wndData->n] = li;
		wndData->n++;
		GlobalUnlock(hCopy);
	}
	else if ((hCopy = GetClipboardData(idRect)) != NULL)
	{
		Rectangles* lcop = (Rectangles*)GlobalLock(hCopy);
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hwndActive, 0);
		HDC hdc = GetDC(hwndActive);
		Rectangles * li = new Rectangles;
		li->A.x = lcop->A.x;
		li->A.y = lcop->A.y;
		li->B.x = lcop->B.x;
		li->B.y = lcop->B.y;
		li->color = lcop->color;
		//li->color = wndData->color;
		SetROP2(hdc, R2_NOTXORPEN);
		li->Draw(hdc);
		li->Draw(hdc);
		ReleaseDC(hwndActive, hdc);
		wndData->obj[wndData->n] = li;
		wndData->n++;
		GlobalUnlock(hCopy);
	}
	else if ((hCopy = GetClipboardData(idElip)) != NULL)
	{
		Ellipses* lcop = (Ellipses*)GlobalLock(hCopy);
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hwndActive, 0);
		HDC hdc = GetDC(hwndActive);
		Ellipses * li = new Ellipses;
		li->A.x = lcop->A.x;
		li->A.y = lcop->A.y;
		li->B.x = lcop->B.x;
		li->B.y = lcop->B.y;
		li->color = lcop->color;
		//li->color = wndData->color;
		SetROP2(hdc, R2_NOTXORPEN);
		li->Draw(hdc);
		li->Draw(hdc);
		ReleaseDC(hwndActive, hdc);
		wndData->obj[wndData->n] = li;
		wndData->n++;
		GlobalUnlock(hCopy);
	}
	else if ((hCopy = GetClipboardData(idText)) != NULL)
	{
		Texts* lcop = (Texts*)GlobalLock(hCopy);
		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hwndActive, 0);
		HDC hdc = GetDC(hwndActive);
		Texts * li = new Texts;
		li->A.x = lcop->A.x;
		li->A.y = lcop->A.y;
		li->B.x = lcop->B.x;
		li->B.y = lcop->B.y;
		li->color = lcop->color;
		li->font = lcop->font;
		wcsncpy_s(li->str, lcop->str, wcslen(lcop->str));
		//li->color = wndData->color;
		SetROP2(hdc, R2_NOTXORPEN);
		li->Draw(hdc);
		li->Draw(hdc);
		ReleaseDC(hwndActive, hdc);
		wndData->obj[wndData->n] = li;
		wndData->n++;
		GlobalUnlock(hCopy);
	}
	else
	{
		if (!OpenClipboard(hWnd))
		{
			MessageBox(hWnd, _T("Open clipboard fail"), _T("Notice"), 0);
			return;
		}
		HANDLE hText = GetClipboardData(CF_UNICODETEXT);
		if (hText == NULL)
		{
			MessageBox(hWnd, _T("Don't have text data in CB"), _T("Notice"), 0);
			CloseClipboard();
			return;
		}

		wchar_t *  pText = (WCHAR*)(GlobalLock(hText));

		if (pText == NULL)
		{
			MessageBox(hWnd, _T("Cast fail"), _T("warning"), 0);
			CloseClipboard();
			return;
		}

		CHILD_WND_DATA *wndData = (CHILD_WND_DATA *)GetWindowLongPtr(hwndActive, 0);
		HDC hdc = GetDC(hwndActive);
		Texts * li = new Texts;
		li->A.x = 0;
		li->A.y = 0;
		li->B.x = 0;
		li->B.y = 0;
		wcsncpy_s(li->str, pText, wcslen(pText));
		li->color = wndData->color;
		li->font = wndData->font;
		SetROP2(hdc, R2_NOTXORPEN);
		li->Draw(hdc);
		li->Draw(hdc);
		ReleaseDC(hwndActive, hdc);
		wndData->obj[wndData->n] = li;
		wndData->n++;
		GlobalUnlock(hText);


	}

	CloseClipboard();
	RECT r;
	GetClientRect(hwndActive, &r);
	InvalidateRect(hwndActive, &r, 1);
	
}

void onDelete(HWND hWnd)
{
	object->A.x = -1;
	object->A.y = -1;
	object->B.x = -1;
	object->B.y = -1;
	RECT r;
	GetClientRect(hwndActive, &r);
	InvalidateRect(hwndActive, &r, 1);
}