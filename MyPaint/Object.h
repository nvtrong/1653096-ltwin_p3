#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include"stdafx.h"
#define MAX_TEXT_LENGTH 200
using namespace std;
struct Point
{
	int x, y;
};
class Object
{
public:
	int type;
	Point A, B;
	Point startPoint;
	Point originalBegin, originalEnd;
	wchar_t  str[MAX_TEXT_LENGTH];
	int count_str;
	LONG lfHeight;
	LOGFONT font;
	COLORREF color;
	Object();
	void Swap(Object *&tmp);
	virtual void Draw(HDC hdc) = 0;
	virtual bool PointInObj(HDC hdc, Point p)=0;
	virtual void SelectRect(HDC hdc)=0;
	void StartPoint(Point &a);
	void MovingObj(Point &cur);
	int CheckMouseIsResizing(Point &cur);
	void ResizingObj(Point &cur, int mode);
};
class Line : public Object
{
public:
	Line();
	void Draw(HDC hdc);
	bool PointInObj(HDC hdc, Point p);
	void SelectRect(HDC hdc);

};
class Rectangles : public Object
{
public:
	Rectangles();
	void Draw(HDC hdc);
	bool PointInObj(HDC hdc, Point p);
	void SelectRect(HDC hdc);
};
class Ellipses : public Object
{
public:
	Ellipses();
	void Draw(HDC hdc);
	bool PointInObj(HDC hdc, Point p);
	void SelectRect(HDC hdc);
};
class Texts : public Object
{
public:
	Texts();
	void Draw(HDC hdc);
	bool PointInObj(HDC hdc, Point p);
	void SelectRect(HDC hdc);
};
