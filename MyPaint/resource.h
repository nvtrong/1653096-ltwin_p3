//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyPaint.rc
//
#define IDC_MYICON                      2
#define IDD_MYPAINT_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_NEW                         106
#define IDI_MYPAINT                     107
#define IDI_SMALL                       108
#define IDC_MYPAINT                     109
#define IDS_APP_TITLE_DRAW              110
#define IDC_DRAW                        111
#define IDM_OPEN                        112
#define IDM_SELECTOBJECT                113
#define IDM_SAVE                        117
#define IDM_RECTANGLE                   118
#define IDM_ELLIPSE                     119
#define IDM_LINE                        120
#define IDM_TEXT                        121
#define IDM_CASCADE                     123
#define IDM_CLOSEALL                    124
#define IDM_TITLE                       125
#define IDM_COLOR                       126
#define IDM_FONT                        127
#define IDR_MAINFRAME                   128
#define IDM_EXIT                        129
#define IDB_DRAW                        129
#define IDM_TIDE                        130
#define IDB_LINE                        131
#define IDB_RECTANGLE                   132
#define IDB_ELLIPSE                     133
#define IDB_TEXT                        134
#define IDB_SELECTOBJECT                135
#define IDD_DIALOG1                     135
#define IDC_INPUTTEXT                   1001
#define IDC_BUTTON2                     1003
#define IDC_OK                          1003
#define IDM_EDITCUT                     32779
#define IDM_EDITCOPY                    32780
#define IDM_EDITPASTE                   32781
#define IDM_EDITDELETE                  32782
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
